<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CartController extends Controller

{
    //this ensures users must be logged in to view the cart
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $order = Auth::user()->cart();

        $products = $order->products;

        return view('cart.index', compact('order', 'products'));
    }

    public function addProduct(Request $request) //request gets the info from the post
    {
        $order = Auth::user()->cart(); //gets what the user has in their cart

        $product_id = $request->input('product_id'); //takes productid from form - input gets data from the post (request object)
        $product = Product::findOrFail($product_id); //gets the product form the product id

        $existingProduct = $order->products()->where('product_id', $product->id)->first(); //trys to get the product form the existing order, if exists add 1 to quantity
        if ($existingProduct) {
            $existingProduct->pivot->quantity += 1;
            $existingProduct->pivot->save(); //updates the quantity
        } else { // if the product is not alreay int he order then set the quantity to 1
            $quantity = 1;
            $order->products()->attach($product->id, ['quantity' => $quantity]);
        }
        return redirect()->route('cart');
    }

    public function removeProduct(Request $request)
    {
        $order = Auth::user()->cart();

        $product_id = $request->input('product_id');
        $product = Product::findOrFail($product_id);

        $order->products()->detach($product->id); //removes the product

        return redirect()->route('cart');
    }

    public function updateQuantities(Request $request)
    {
        $quantities = $request->input('quantity');

        $products = [];
        foreach($quantities as $product_id => $quantity) {
            if ($quantity > 0) {
                $products[$product_id] = ['quantity' => $quantity];
            }
        }

        $order = Auth::user()->cart();
        $order->products()->sync($products);

        return redirect()->route('cart');
    }

}
