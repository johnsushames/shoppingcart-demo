<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products') //get products but go through order_products table
            ->withPivot('quantity','price')
            ->withTimestamps();
    }
}
