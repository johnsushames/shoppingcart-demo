<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('order_status', ['cart', 'finalised', 'shipped']);
            $table->decimal('price', 6, 2);
            $table->text('shipping_address');
            $table->string('shipping_method');
            $table->boolean('payment_received')->default(0);
            $table->string('payment_reference');
            $table->timestamps();

             $table->foreign('user_id')
             ->references('id')->on('users')
             ->onUpdate('cascade')
             ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
