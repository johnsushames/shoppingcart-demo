@extends('layouts.master')

@section('content')



  <div class="container">
  <ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="{{ route('products.index') }}">Products</a></li>
  <li class="active">{{ $product->name }}</li>
</ol>


    <h1>{{ $product->name }}</h1>
    <p>${{ $product->price }}</p>
    <p>{{ $product->description }}</p>
    {{-- <p>{{ $product->id }}</p> --}}
    {{-- <a href="{{ route('products.index') }}"><button class="btn btn-primary btn-sm">Back to Products</button></a> --}}

    @if(Auth::check())
      {!! Form::open(['route' => 'cart.add', 'class' => 'form-horizontal']) !!}
        {!! Form::hidden('product_id', $product->id) !!}
        <button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add to Cart</button>
      {!! Form::close() !!}
    @endif
  </div>

@endsection




