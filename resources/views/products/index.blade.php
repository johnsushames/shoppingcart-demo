@extends('layouts.master')

@section('content')

  <div class="container">
    <h1>Products</h1>
    <ul>
      @foreach($products as $product)
        <li>
            <a href="{{ route('products.show', $product->id) }}">
                {{ $product->name }}
            </a>
            {{-- - {{ $mission->user->name }} --}}
            {{-- - {{ $mission->user->email }} --}}
        </li>
      @endforeach
    </ul>
    {{-- <p><a href="{{ route('missions.create') }}" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Create Mission</a></p> --}}
  </div>
@endsection
